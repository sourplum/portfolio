<?php

class Project {

    static public $nextId = 1;
    public $id;

    private $title;
    private $picture;
    private $description;
    private $download;
    private $contributor;
    private $date;
    private $language;
    private $framework;

    private $launcher_preview;
    private $isCarousel = false;
    public $carousel = array();

    function setCarousel($carousel) {
    	$this->isCarousel = true;
    	$this->carousel = $carousel;
    }

    function __construct($title, $picture, $download, $contributor, $date, $language, $framework = null) {
        $this->id = self::$nextId++;
        $this->title = $title;
        $this->picture = $picture;
        $this->download = $download;
        $this->contributor = $contributor;
        $this->date = $date;
        $this->language = $language;
        $this->framework = $framework;
    }

    public function fillDescription($description) {
$this->description = $description;
    }

    public function setPreview($launcher_preview) {
$this->launcher_preview = $launcher_preview;
    }


    public function displayLauncher() {
echo <<<EOT
        <div class="col-sm-4 portfolio-item">
            <a href="#project_$this->id" class="portfolio-link" data-toggle="modal">
                <div class="caption">
                    <div class="caption-content">
                        <i class="fa fa-search-plus fa-3x"></i>
                    </div>
				</div>
                <img src="$this->launcher_preview" class="img-responsive" alt="">
            </a>
        </div>
EOT;
    }

    private function displayCarousel() {
    	echo '<div id="carousel-example-generic" class="carousel slide" data-ride="carousel"><ol class="carousel-indicators">';

    	foreach ($this->carousel as $key => $value) {
    		if ($key == 0) {
    			echo '<li data-target="#carousel-example-generic" data-slide-to="'.$key.'" class="active"></li>';
    		} else {
    			echo '<li data-target="#carousel-example-generic" data-slide-to="'.$key.'"></li>';
    		}
    	}
    	echo '</ol><div class="carousel-inner">';

		foreach ($this->carousel as $key => $value) {
			$picAndCaption = explode(" :", $value);
			if ($key == 0) {
				echo '<div class="item active">';
			} else {
				echo '<div class="item">';
			}
			echo '<img src="'.$picAndCaption[0].'" class="img-responsive img-centered" alt=""><div class="carousel-caption">'.$picAndCaption[1].'</div></div>';
		}
		echo '</div></div></div>';
    }

    public function display() {
echo <<<EOT
<div class="portfolio-modal modal fade" id="project_$this->id" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="modal-body">
                                <h2>$this->title</h2>
                                <hr class="star-primary">
EOT;
if ($this->isCarousel == true) {
	$this->displayCarousel();
} else {
	echo <<<EOT
	<img src="$this->picture" class="img-responsive img-centered" alt="">
EOT;
}
echo <<<EOT
                                <p>$this->description</p>
                                <a href="$this->download" class="btn btn-lg btn-primary">
                                    <i class="fa fa-download"></i> Download
                                </a>
                                <ul class="list-inline item-details">
                                    <li>Contributor:
                                        <strong>$this->contributor</strong>
                                    </li>
                                    <li>Date:
                                        <strong>$this->date</strong>
                                    </li>
                                    <li>Language:
                                        <strong>$this->language</strong>
                                    </li>
                                    <li>Library:
                                        <strong>$this->framework</strong>
                                    </li>
                                </ul>

                                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
EOT;
    }
}

$projects = array();

// Dumb Rock Rocks
array_push($projects, new Project("Dumb Rock Rocks",'img/projects/dumbrockrocksdemo.png',
    'https://bitbucket.org/cedbonhomme/purupuru', '<a href="https://bitbucket.org/chloe__d">Chloé Daurient</a>',
    'March 2014 – May 2014','C++ 11', 'SFML') );

end($projects)->fillDescription(
<<<EOT
    Inspired by <a href="http://www.bigfishgames.fr/jeux-en-ligne/8638/puru-puru-digger/index.html">a flash game</a>
    this version was made using C++ and SFML graphic library.
    this project was the first where we were introduced object-oriented programming.
    The game let the user move a rock (the main character) on a grid by clicking on nearby cells.
    Each cell has a number on it that let the character move of n cells, n being the number.
    The user loose the game if he goes out of the grid, if he touch a bomb or if he's too slow to finish the level.
    The game is delivered with a complete Doxygen documentation and the source code.
EOT
);
end($projects)->setPreview('img/projects/dumbrockrocksdemo.png');

// I don't care I'll just buy another LimoPong
array_push($projects, new Project("I don't care I'll just buy another LimoPong",'img/projects/pong.png',
    'https://bitbucket.org/cedbonhomme/pong', '<a href="#">Corentin Bienassis</a>',
    'September 2013 – November 2013','C++ 11', '<a href="https://www.libsdl.org/">SDL</a>') );

end($projects)->fillDescription(
<<<EOT
    Programming 101. A classic among classics to learn basic C++ while working in collaboration using the subversion tool Mercurial.
EOT
);
end($projects)->setPreview('img/projects/pong.png');

// Swag HatVenture
array_push($projects, new Project("Swag HatVenture", 'img/projects/swaghatventure/swaghatventuregreencrop.png',
    'https://bitbucket.org/cedbonhomme/monster','<a href="#">Corentin Bienassis</a>',
    'October 2013 – December 2013', 'C++ 11', '<a href="https://www.libsdl.org/">SDL</a>') );
end($projects)->fillDescription(
<<<EOT
    Inspired by the mobile game <a href="http://www.puddingmonsters.com/">pudding monster</a>,
    this version was made using C++ and the SDL graphic library.
    The game let the user play to 9 levels were the goal is to wake up sleepy monsters by moving the main character on a 8*8 grid.
    A level editor has been added so that new levels could be made.
    Hightscores are also avaible on the menu.
EOT
);
end($projects)->setPreview('img/projects/swaghatventure/swaghatventuregreencrop.png');

end($projects)->setCarousel(array('img/projects/swaghatventure/swaghatventuregreen.png :awesome',
	'img/projects/swaghatventure/swaghatventureice.png :cool',
	'img/projects/swaghatventure/swaghatventureice2.png :rad',
	'img/projects/swaghatventure/swaghatventurelava.png :neetos',
	  'img/projects/swaghatventure/swaghatventuremenu.png :mathematical',
	  'img/projects/swaghatventure/swaghatventureedit.png :super neetos'));

// Sliding Game
array_push($projects, new Project("Taquin",'img/projects/taquin.png',
    'https://bitbucket.org/Thibaud_Laurent/projet-iuts3-progjava-taquin', '<a href="https://bitbucket.org/Thibaud_Laurent">Thibaud Laurent</a>',
    'November 2014 _ December 2014', 'Java', 'Swing') );
end($projects)->fillDescription(
<<<EOT
    A fun sliding puzzle Game made with Java components.
EOT
);
end($projects)->setPreview('img/projects/taquin.png');

// QLetItGraph
array_push($projects, new Project("QLetItGraph", 'img/projects/QLetItGraph.png',
    'https://bitbucket.org/cedbonhomme/qletitgraph', '',
    'November 2014 _ WIP', 'Qt/C++', 'Qt OpenGL'));
end($projects)->fillDescription(
<<<EOT
    Qt Application using Opengl to render a 3d element controlled by the user with mouse and sliders.
    This is my first Qt Application and its purpose is for me to learn more about this beautiful framework and about its OpenGL integration.
EOT
);
end($projects)->setPreview('img/projects/QLetItGraph.png');

//Mozartzon
array_push($projects, new Project("Mozartzon", 'img/projects/mozartzon.png',
    'http://mozartzon.biz.st','<a href="#">Thibaud Laurent</a>',
    'November 2014 _ WIP', 'Php','Microsoft SQL Server'));
end($projects)->setPreview('img/projects/mozartzon.png');
end($projects)->fillDescription(
    <<<EOT
Listen to your favorite classical musician on <a href="http://mozartzon.biz.st">Mozartzon</a> the biggest eCommerce website for classical music.
EOT
);

// HTML generation
foreach ($projects as $project) {
    $project->display();
}

?>

<!-- Portfolio Grid Section -->
        <section id="portfolio">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2>Portfolio</h2>
                        <hr class="star-primary">
                    </div>
                </div>
                <div class="row">
                    <?php
                    foreach ($projects as $project) {
                        $project->displayLauncher();
                    }
                    ?>
                </div>
            </div>
        </section>
